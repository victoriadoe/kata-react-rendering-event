import './App.css'
import { Calendar } from './components/calendar/Calendar'
import { ErrorBoundary } from 'react-error-boundary'
import { ErrorHandler } from './components/error/Error-Boundary'


function App() {
    return (
        <div className="App">
            <ErrorBoundary FallbackComponent={ErrorHandler}>
                <Calendar />
            </ErrorBoundary>
        </div>
    )

}

export default App

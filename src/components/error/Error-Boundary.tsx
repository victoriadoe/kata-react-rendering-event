export const ErrorHandler = ({ error }: { error: Error }) => {
    return (
        <div role="alert">
            <p>An error occurred:</p>
            <pre>{error.message}</pre>
        </div>
    )
}

import { getHours } from '../../utils/utils'
import '../../styles/time.css'

export const Time = () => {
    const hours = getHours()

    return (
        <ul className="relative ml-4 text-pink-900">
            {hours.map((h, index) => {
                return (
                    <li key={index} style={{ position: 'absolute', top: `${56 * index}px` }}>
                        {h}
                    </li>
                )
            })}
        </ul>
    )
}
